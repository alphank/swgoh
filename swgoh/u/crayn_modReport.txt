                   Speed  130    1.00   16.00    4.95 
   CriticalChancePercent   74    0.63    3.82    1.74 
          PotencyPercent   81    0.51    7.78    1.90 
         TenacityPercent   69    0.52    5.04    1.60 
                 Offense   81    9.00  110.00   35.10 
                 Defense   80    2.00   20.00    6.68 
                  Health   69  122.00  934.00  346.12 
              Protection   83  101.00 2759.00  706.11 
          OffensePercent   58    0.12    1.63    0.46 
          DefensePercent   65    0.57    4.58    1.52 
           HealthPercent   67    0.30    3.67    0.95 
       ProtectionPercent   64    0.50    7.22    2.15 
