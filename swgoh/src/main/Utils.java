package main;

import java.util.HashMap;

/**
 * @author Alphan
 *
 */
public class Utils {

	/**
	 * @param lsToonNames
	 * @return
	 */
	public static HashMap<String, Integer> populateToonIdsHashMap(String[] lsToonNames) {
		HashMap<String, Integer> hmToonIds = new HashMap<String, Integer>();
		
		for (int idx = 0; idx < lsToonNames.length; idx++) {
			hmToonIds.put(lsToonNames[idx], idx);
		}
		
		return hmToonIds;
	}
	
	
	/**
	 * @param lsRomanNumbers
	 * @return
	 */
	public static HashMap<String, Integer> populateRomanNumbersHashMap(String[] lsRomanNumbers) {
		HashMap<String, Integer> hmRomanNumbers = new HashMap<String, Integer>();
		
		for (int idx = 0; idx < lsRomanNumbers.length; idx++) {
			hmRomanNumbers.put(lsRomanNumbers[idx], idx);
		}
		
		return hmRomanNumbers;

	}
	
}
