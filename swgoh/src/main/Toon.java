package main;


/**
 * @author Alphan
 *
 */
public class Toon {
	public String playerName;
	public String toonName;
	public int toonId;
	public int starCount;
	public int charLevel;
	public String gearLevelStr;
	public int gearLevel;
	public int power;
}
