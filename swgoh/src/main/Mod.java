package main;

public class Mod {
	
	private static final int MAX_NUM_OF_SEC_STATS = 4;
	
	public SlotType slotType;
	public SetType setType;
	
	public int pips;
	public int level;
	
	public PrimaryStat priStat;
	public String priStatValStr;
	public double priStatVal;
	
	
	public SecondaryStat[] secStat;
	public String[] secStatValStr;
	public double[] secStatVal;

	public String toon;
	public int toonId;
	
	public double score;
	
	/**
	 * 
	 */
	public Mod() {
		secStat = new SecondaryStat[MAX_NUM_OF_SEC_STATS];
		secStatValStr = new String[MAX_NUM_OF_SEC_STATS];
		secStatVal = new double[MAX_NUM_OF_SEC_STATS];
	}
	
	enum SlotType {
		Square,
		Arrow,
		Diamond,
		Triangle,
		Circle,
		Cross
	}
	
	public static final String[] SLOT_TYPES = {
		"Transmitter",
		"Receiver",
		"Processor",
		"Holo-Array",
		"Data-Bus",
		"Multiplexer"
	};
	
	/**
	 * @param slotTypeStr
	 * @return
	 */
	public static SlotType getSlotTypeFromString(String slotTypeStr) {
		
		for (int idx = 0; idx < SLOT_TYPES.length; idx++) {
			if (slotTypeStr.indexOf(SLOT_TYPES[idx]) != -1) {
				return SlotType.values()[idx];
			}
		}
		return null;
	}
	
	enum SetType {
		Health,
		Defense,
		Crit_Damage,
		Crit_Chance,
		Tenacity,
		Offense,
		Potency,
		Speed
	}
	
	/**
	 * @param setTypeStr
	 * @return
	 */
	public static SetType getSetTypeFromString(String setTypeStr) {

		setTypeStr = setTypeStr.replace(" ", "_");
		SetType[] setTypes = SetType.values();
		
		for (SetType setType : setTypes) {
			if (setTypeStr.indexOf(setType.toString()) != -1) {
				return setType;
			}
		}
		return null;
	}
	
	enum PrimaryStat {
		Speed,
		CriticalChance,
		CriticalDamage,
		Potency,
		Tenacity,
		Accuracy,
		CriticalAvoidance,
		Offense,
		Defense,
		Health,
		Protection
	}
	
	/**
	 * @param priStatStr
	 * @param isPercent
	 * @return
	 */
	public static PrimaryStat getPrimaryStatFromString(
			String priStatStr, boolean isPercent) {
		
		priStatStr = priStatStr.replace(" ", "");
		PrimaryStat[] primaryStats = PrimaryStat.values();
		
		for (PrimaryStat primaryStat : primaryStats) {
			if (priStatStr.equals(primaryStat.toString())) {
				return primaryStat;
			}
		}
		return null;
	}
	
	enum SecondaryStat {
		Speed,
		CriticalChancePercent,
		PotencyPercent,
		TenacityPercent,
		Offense,
		Defense,
		Health,
		Protection,
		OffensePercent,
		DefensePercent,
		HealthPercent,
		ProtectionPercent
	}
	
	/**
	 * @param secStatStr
	 * @param isPercent
	 * @return
	 */
	public static SecondaryStat getSecondaryStatFromString(
			String secStatStr, boolean isPercent) {
		
		secStatStr = secStatStr.replace(" ", "");
		if (isPercent) {
			secStatStr += "Percent";
		}
		SecondaryStat[] secondaryStats = SecondaryStat.values();
		
		for (SecondaryStat secondaryStat : secondaryStats) {
			if (secStatStr.equals(secondaryStat.toString())) {
				return secondaryStat;
			}
		}
		return null;
	}
	
	/**
	 * @param valueStr
	 * @return
	 */
	public static double getValueFromStringAsDouble(String valueStr) {
		
		valueStr = valueStr.replace("+", "");
		valueStr = valueStr.replace("%", "");
		
		return Double.parseDouble(valueStr);
	}

}
