package main;

public class TextRom {

	/**
	 * 
	 */
	public static final String[] ToonNames = {
			"aayla-secura",
			"admiral-ackbar",
			"ahsoka-tano",
			"ahsoka-tano-fulcrum",
			"amilyn-holdo",
			"asajj-ventress",
			"b2-super-battle-droid",
			"barriss-offee",
			"baze-malbus",
			"bb-8",
			"biggs-darklighter",
			"bistan",
			"boba-fett",
			"bodhi-rook",
			"bossk",
			"cad-bane",
			"captain-han-solo",
			"captain-phasma",
			"cassian-andor",
			"cc-2224-cody",
			"chief-chirpa",
			"chief-nebit",
			"chirrut-imwe",
			"chopper",
			"clone-sergeant-phase-i",
			"clone-wars-chewbacca",
			"colonel-starck",
			"commander-luke-skywalker",
			"coruscant-underworld-police",
			"count-dooku",
			"ct-21-0408-echo",
			"ct-5555-fives",
			"ct-7567-rex",
			"darth-maul",
			"darth-nihilus",
			"darth-sidious",
			"darth-sion",
			"darth-traya",
			"darth-vader",
			"dathcha",
			"death-trooper",
			"dengar",
			"director-krennic",
			"eeth-koth",
			"emperor-palpatine",
			"ewok-elder",
			"ewok-scout",
			"ezra-bridger",
			"finn",
			"first-order-executioner",
			"first-order-officer",
			"first-order-sf-tie-pilot",
			"first-order-stormtrooper",
			"first-order-tie-pilot",
			"gamorrean-guard",
			"garazeb-zeb-orrelios",
			"gar-saxon",
			"general-grievous",
			"general-kenobi",
			"general-veers",
			"geonosian-soldier",
			"geonosian-spy",
			"grand-admiral-thrawn",
			"grand-master-yoda",
			"grand-moff-tarkin",
			"greedo",
			"han-solo",
			"hera-syndulla",
			"hermit-yoda",
			"hk-47",
			"hoth-rebel-scout",
			"hoth-rebel-soldier",
			"ig-100-magnaguard",
			"ig-86-sentinel-droid",
			"ig-88",
			"ima-gun-di",
			"imperial-probe-droid",
			"imperial-super-commando",
			"jawa",
			"jawa-engineer",
			"jawa-scavenger",
			"jedi-consular",
			"jedi-knight-anakin",
			"jedi-knight-guardian",
			"jyn-erso",
			"k-2so",
			"kanan-jarrus",
			"kit-fisto",
			"kylo-ren",
			"kylo-ren-unmasked",
			"lando-calrissian",
			"lobot",
			"logray",
			"luke-skywalker-farmboy",
			"luminara-unduli",
			"mace-windu",
			"magmatrooper",
			"mob-enforcer",
			"mother-talzin",
			"nightsister-acolyte",
			"nightsister-initiate",
			"nightsister-spirit",
			"nightsister-zombie",
			"nute-gunray",
			"obi-wan-kenobi-old-ben",
			"old-daka",
			"pao",
			"paploo",
			"plo-koon",
			"poe-dameron",
			"poggle-the-lesser",
			"princess-leia",
			"qui-gon-jinn",
			"r2-d2",
			"rebel-officer-leia-organa",
			"resistance-pilot",
			"resistance-trooper",
			"rey-jedi-training",
			"rey-scavenger",
			"rose-tico",
			"royal-guard",
			"sabine-wren",
			"savage-opress",
			"scarif-rebel-pathfinder",
			"shoretrooper",
			"sith-assassin",
			"sith-marauder",
			"sith-trooper",
			"snowtrooper",
			"stormtrooper",
			"stormtrooper-han",
			"sun-fac",
			"talia",
			"teebo",
			"tie-fighter-pilot",
			"tusken-raider",
			"tusken-shaman",
			"ugnaught",
			"urorrurrr",
			"veteran-smuggler-chewbacca",
			"veteran-smuggler-han-solo",
			"visas-marr",
			"wampa",
			"wedge-antilles",
			"wicket",
			"zam-wesell"
	};

	/**
	 * 
	 */
	public static final String[] RomanNumbers = {
			"-",
			"I",
			"II",
			"III",
			"IV",
			"V",
			"VI",
			"VII",
			"VIII",
			"IX",
			"X",
			"XI",
			"XII"
	};
}
