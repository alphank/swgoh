package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Alphan
 *
 */
public class RosterCheck {

	public static final String GUILD_URL = "https://swgoh.gg/g/19683/the-last-days-of-sun/";
	public static final String URL_STRING = "https://swgoh.gg%scollection/";

	private static final Object START_PREAMBLE = "<li class=\"media list-group-item p-a collection-char-list\">";
	private static final Object BREAK_PREAMBLE = "</li>";

	private static final Object GUILD_START_PREAMBLE = "<tbody>";
	private static final Object GUILD_BREAK_PREAMBLE = "</tbody>";



	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		boolean updateFromSwgoh = false;

		HashMap<String, String> hmPlayers = getGuildPlayerList(GUILD_URL, updateFromSwgoh);

		HashMap<String, Integer> hmToonIds = Utils.populateToonIdsHashMap(TextRom.ToonNames);

		HashMap<String, Integer> hmRomanNumbers = Utils.populateRomanNumbersHashMap(TextRom.RomanNumbers);


		//String playerName = "/u/mrsidious";

		HashMap<String, ArrayList<Toon>> hmGuildRoster = new HashMap<String, ArrayList<Toon>>();

		for (String name : hmPlayers.keySet()) {
			String playerUrl = hmPlayers.get(name);

			ArrayList<Toon> lsToon = getRosterForPlayer(playerUrl, updateFromSwgoh, hmToonIds, hmRomanNumbers);

			hmGuildRoster.put(name, lsToon);
		} 


		int numOfPlayers = hmPlayers.size();
		int numOfToons = hmToonIds.size();
		
		System.out.println("numOfPlayers = " + numOfPlayers);
		System.out.println("numOfToons = " + numOfToons);

		int[][] guildRoster = new int[numOfToons][numOfPlayers];

		List<String> players = new ArrayList<>(hmPlayers.keySet());

		for (int pIdx = 0; pIdx < players.size(); pIdx++) {

			ArrayList<Toon> toons = hmGuildRoster.get(players.get(pIdx));

			//System.out.println(players.get(pIdx) + " : ");
			for (Toon toon : toons) {
				guildRoster[toon.toonId][pIdx] = toon.starCount;
				//System.out.print(toon.toonId + " " + toon.toonName + "(" + toon.starCount + "), ");
			}
			//System.out.println();
		}
		
		
		for (int idx = 0; idx < numOfPlayers; idx++) {
			System.out.print(players.get(idx) + "\t");
		}
		System.out.println();
		for (int r = 0; r < numOfToons; r++) {
			for (int c = 0; c < numOfPlayers; c++) {
				System.out.print(guildRoster[r][c] + "\t");
			}
			System.out.println();
		}

		
		/*
		for (Toon toon : lsToon) {
        	System.out.println(toon.starCount + " : " + toon.toonName);
		}
		 */
	}


	/**
	 * @param guildUrl
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, String> getGuildPlayerList(String guildUrl, boolean updatePlayers) throws Exception {

		HashMap<String, String> hmPlayers = new HashMap<String, String>();
		File tempFile = new File("guildPlayerList.tmp");
		
		if (updatePlayers) {
			URL url = new URL(guildUrl);

			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");

			InputStream isUrl = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(isUrl));
			FileWriter fwTemp = new FileWriter(tempFile);
			BufferedWriter bwTemp = new BufferedWriter(fwTemp);

			String line = null;
			boolean isStarted = false;

			while ((line = br.readLine()) != null) {
				line = line.trim();

				if (line.equals(GUILD_START_PREAMBLE)) {
					isStarted = true;
				}

				if (isStarted == false) {
					continue;
				}

				if (line.equals(GUILD_BREAK_PREAMBLE)) {
					break;
				}

				if (line.indexOf("<img class=") != -1) {
					continue;
				}

				if (isStarted) {
					bwTemp.write(line + "\n");
				}
			}

			if (isStarted == false) {
				bwTemp.close();
				fwTemp.close();

				System.out.println("Stream Error");
				throw new Exception();
			}
			bwTemp.write(GUILD_BREAK_PREAMBLE + "\n");

			bwTemp.close();
			fwTemp.close();
		}


		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.parse(tempFile);

		document.getDocumentElement().normalize();

		Element elmTbody = document.getDocumentElement();

		NodeList nodeList = elmTbody.getElementsByTagName("tr");

		for (int idx = 0; idx < nodeList.getLength(); idx++) {
			Node node = nodeList.item(idx);

			Element elmTr = (Element) node;
			Element elmTd = getFirstChildElement(elmTr);

			String playerName = elmTd.getAttribute("data-sort-value");

			Element elmHref = getFirstChildElement(elmTd);

			String playerUrl = elmHref.getAttribute("href");

			hmPlayers.put(playerName, playerUrl);

			System.out.println(playerName + " : " + playerUrl);
		}

		return hmPlayers;
	}

	/**
	 * @param playerName
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<Toon> getRosterForPlayer(String playerName, boolean updateFromSwgoh,
			HashMap<String, Integer> hmToonIds, HashMap<String, Integer> hmRomanNumbers) 
					throws Exception {
		
		ArrayList<Toon> lsToon = new ArrayList<Toon>();
		File tempFile = new File(playerName.substring(1, playerName.length() - 1) + "_roster.tmp");
		
		if (updateFromSwgoh) {
			String sUrl = String.format(URL_STRING, playerName);

			URL url = new URL(sUrl);

			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");

			InputStream isUrl = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(isUrl));

			FileWriter fwTemp = new FileWriter(tempFile);
			BufferedWriter bwTemp = new BufferedWriter(fwTemp);

			String line = null;
			boolean isStarted = false;

			while ((line = br.readLine()) != null) {
				line = line.trim();

				if (line.equals(START_PREAMBLE)) {
					isStarted = true;
				}

				if (isStarted == false) {
					continue;
				}

				if (line.equals(BREAK_PREAMBLE)) {
					break;
				}

				if (line.indexOf("<img class=") != -1) {
					continue;
				}

				if (isStarted) {
					bwTemp.write(line + "\n");
				}
			}

			if (isStarted == false) {
				bwTemp.close();
				fwTemp.close();

				System.out.println("Stream Error");
				throw new Exception();
			}
			bwTemp.write(BREAK_PREAMBLE + "\n");

			isUrl.close();

			bwTemp.close();
			fwTemp.close();
		}


		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.parse(tempFile);

		document.getDocumentElement().normalize();

		Element element = document.getDocumentElement();
		Element elmDivRow = (Element) element.getElementsByTagName("div").item(0);


		NodeList nodeList = elmDivRow.getChildNodes();

		//NodeList nodeList = elmDivRow.getElementsByTagName("div");
		System.out.println("Char Count = " + nodeList.getLength());

		for (int idx = 0; idx < nodeList.getLength(); idx++) {
			Node node = nodeList.item(idx);

			if (node.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			Element elmDivCollection = getFirstChildElement((Element)node);
			Element elmDivPortrait = getFirstChildElement((Element)elmDivCollection);
			Element elmAHref = getFirstChildElement(elmDivPortrait);

			Toon toon = new Toon();

			toon.playerName = playerName;

			String name = getToonNameFromHref(elmAHref.getAttribute("href"));
			toon.toonName = name;
			toon.toonId = hmToonIds.get(name);

			if (name.equals("wampa")) {
				//System.out.println("Debug");
			}

			if (isToonActivated(elmDivCollection)) {
				getStarCount(elmAHref, toon);
				toon.gearLevel = hmRomanNumbers.get(toon.gearLevelStr);
			}
			else {
				toon.starCount = 0;
			}
			lsToon.add(toon);
		}

		return lsToon;
	}


	/**
	 * @return
	 */
	public static boolean isToonActivated(Element elmDivCollection) {

		if (elmDivCollection.getAttribute("class").indexOf("collection-char-missing") == -1) {
			return true;
		}
		else {
			return false;
		}

	}

	/**
	 * @param elmDivCollection
	 * @return
	 */
	public static void getStarCount(Element elmAHref, Toon toon) {

		NodeList nodeList = elmAHref.getChildNodes();
		int stars = 0;

		for (int idx = 0; idx < nodeList.getLength(); idx++) {
			Node node = nodeList.item(idx);

			if (node.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			Element elm = (Element) node;

			String sClass = elm.getAttribute("class");

			if (sClass.indexOf("char-portrait-full-level") != -1) {
				toon.charLevel = Integer.parseInt(elm.getTextContent());
				continue;
			}

			if (sClass.indexOf("char-portrait-full-gear-level") != -1) {
				toon.gearLevelStr = elm.getTextContent();
				continue;
			}

			if ((sClass.indexOf("star") != -1) && (sClass.indexOf("star-inactive") == -1)) {
				stars++;
			}
		}

		toon.starCount = stars;
	}


	/**
	 * @param hrefStr
	 * @return
	 */
	public static String getToonNameFromHref(String hrefStr) {
		String[] temp = hrefStr.split("/");

		return temp[temp.length - 1];
	}


	/**
	 * @param element
	 * @return
	 */
	public static Element getFirstChildElement(Element element) {
		Node node = element.getFirstChild();

		while (node.getNodeType() != Node.ELEMENT_NODE) {
			node = node.getNextSibling();
		}

		return (Element)node;
	}

}
