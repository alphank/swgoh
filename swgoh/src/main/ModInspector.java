package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import main.Mod.SecondaryStat;

public class ModInspector {

	public static final String URL_STRING = "https://swgoh.gg%smods/?page=%d";

	private static final Object MODS_START_PREAMBLE = "<li class=\"media list-group-item p-a collection-mod-list\">";
	private static final Object MODS_BREAK_PREAMBLE = "</li>";

	private static final int MAX_NUM_OF_PAGES = 50;


	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		String sPlayer = "/u/bigox/";
		
		//ArrayList<Mod> lsMods = getModsForPlayer("/u/bigox/", false);
		ArrayList<Mod> lsMods = getModsForPlayer(sPlayer, false);

		ModReport mr = generateReportForModsList(lsMods, sPlayer);
		
		evaluateMods(lsMods, mr, sPlayer);

	}

	
	
	/**
	 * @param lsMods
	 * @param mr
	 * @throws IOException 
	 */
	public static void evaluateMods(ArrayList<Mod> lsMods, ModReport mr, String sPlayer) throws IOException {
		
		File file = new File(sPlayer.substring(1, sPlayer.length() - 1) + "_mods.txt");
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		
		for (Mod mod : lsMods) {

			String sModStats = String.format("\t%s\t%.2f", mod.priStat.toString(), mod.priStatVal);
			
			for (int idx = 0; idx < mod.secStat.length; idx++) {
				if (mod.secStat[idx] == null) {
					break;
				}
				
				int ssId = mod.secStat[idx].ordinal();
				double curVal = mod.secStatVal[idx];

				mod.score += curVal / mr.avgValArr[ssId];
				
				sModStats += String.format("\t%s\t%.2f", mod.secStat[idx].toString(), mod.secStatVal[idx]);
			}

			String sModScore = String.format("%28s %2d %12s %12s %7.2f", mod.toon, mod.level,
					mod.slotType.toString(), mod.setType.toString(), mod.score);
			System.out.println(sModScore);
			
			sModScore = String.format("%s\t%d\t%s\t%s\t%.2f", mod.toon, mod.level,
					mod.slotType.toString(), mod.setType.toString(), mod.score);
			
			bw.write(sModScore + sModStats + "\n");
		}
		
		bw.close();
		fw.close();
	}


	/**
	 * @param lsMods
	 * @throws IOException 
	 */
	public static ModReport generateReportForModsList(ArrayList<Mod> lsMods, String sPlayer) throws IOException {
		
		File file = new File(sPlayer.substring(1, sPlayer.length() - 1) + "_modReport.txt");
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		

		ModReport mr = new ModReport();
		
		for (int idx = 0; idx < mr.minValArr.length; idx++) {
			mr.minValArr[idx] = Double.MAX_VALUE;
		}

		for (Mod mod : lsMods) {

			for (int idx = 0; idx < mod.secStat.length; idx++) {
				if (mod.secStat[idx] == null) {
					break;
				}

				int ssId = mod.secStat[idx].ordinal();
				double curVal = mod.secStatVal[idx];

				mr.cntValArr[ssId]++;
				mr.tolValArr[ssId] += curVal;
				mr.minValArr[ssId] = (mr.minValArr[ssId] > curVal) ? curVal : mr.minValArr[ssId];
				mr.maxValArr[ssId] = (mr.maxValArr[ssId] < curVal) ? curVal : mr.maxValArr[ssId];
			}
		}

		String sHeader = String.format("%24s %4s %7s %7s %7s", "Secondary Stat Name", 
				"Cnt", "Min", "Max", "Avg");
		System.out.println(sHeader);
		for (int idx = 0; idx < SecondaryStat.values().length; idx++) {
			if (mr.cntValArr[idx] > 0) {
				mr.avgValArr[idx] = mr.tolValArr[idx] / mr.cntValArr[idx];
			}

			String sReport = String.format("%24s %4d %7.2f %7.2f %7.2f ", SecondaryStat.values()[idx].toString(), 
					mr.cntValArr[idx], mr.minValArr[idx], mr.maxValArr[idx], mr.avgValArr[idx]);
			System.out.println(sReport);
			bw.write(sReport + "\n");
		}
		
		bw.close();
		fw.close();
		
		return mr;
	}

	/**
	 * @param playerName
	 * @param updateFromSwgoh
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<Mod> getModsForPlayer(String playerName, boolean updateFromSwgoh) throws Exception {

		ArrayList<Mod> lsMods = new ArrayList<Mod>();
		int pageIdx = 0;

		while (pageIdx < MAX_NUM_OF_PAGES) {
			pageIdx++;
			File tempFile = new File(playerName.substring(1, playerName.length() - 1) + "_mods_" + pageIdx + ".tmp");

			if (updateFromSwgoh) {
				FileWriter fwTemp = new FileWriter(tempFile);
				BufferedWriter bwTemp = new BufferedWriter(fwTemp);

				String sUrl = String.format(URL_STRING, playerName, pageIdx);

				URL url = new URL(sUrl);

				URLConnection urlConnection = url.openConnection();
				urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");

				InputStream isUrl = null;
				try {
					isUrl = urlConnection.getInputStream();
				}
				catch (Exception e) {
				}
				if (isUrl == null) {
					break;
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(isUrl));

				String line = null;
				boolean isStarted = false;

				while ((line = br.readLine()) != null) {
					line = line.trim();

					if (line.equals(MODS_START_PREAMBLE)) {
						isStarted = true;
					}

					if (isStarted == false) {
						continue;
					}

					if (line.equals(MODS_BREAK_PREAMBLE)) {
						break;
					}

					if (isStarted) {
						bwTemp.write(line + "\n");
					}
				}

				if (isStarted == false) {
					bwTemp.close();
					fwTemp.close();

					System.out.println("Stream Error");
					throw new Exception();
				}
				bwTemp.write(MODS_BREAK_PREAMBLE + "\n");

				isUrl.close();

				bwTemp.close();
				fwTemp.close();
			}
			else {
				if (tempFile.exists() == false) {
					break;
				}
			}

			Document doc = Jsoup.parse(tempFile, "UTF-8");
			Element elmBody = doc.body();

			Elements elmMods = elmBody.getElementsByClass("collection-mod");

			for (Element elmMod : elmMods) {

				Mod mod = new Mod();

				Element elmPips = elmMod.getElementsByClass("statmod-pips").first();
				int pips = elmPips.getElementsByClass("statmod-pip").size();
				mod.pips = pips;			

				Element elmLevel = elmMod.getElementsByClass("statmod-level").first();
				int level = Integer.parseInt(elmLevel.text());
				mod.level = level;

				Element elmToon = elmMod.getElementsByClass("char-portrait").first();
				String toonName = elmToon.attr("title");
				mod.toon = toonName;

				Element elmType = elmMod.getElementsByClass("statmod-img").first();
				String modType = elmType.attr("alt");
				mod.slotType = Mod.getSlotTypeFromString(modType);
				mod.setType = Mod.getSetTypeFromString(modType);


				Element elmPrim = elmMod.getElementsByClass("statmod-stats-1").first();
				String primStatVal = elmPrim.getElementsByClass("statmod-stat-value").first().text();
				String primStatTyp = elmPrim.getElementsByClass("statmod-stat-label").first().text();
				boolean isPercent = false;
				if (primStatVal.indexOf("%") != -1) {
					isPercent = true;
				}
				mod.priStat = Mod.getPrimaryStatFromString(primStatTyp, isPercent);
				mod.priStatVal = Mod.getValueFromStringAsDouble(primStatVal);


				Element elmSeco = elmMod.getElementsByClass("statmod-stats-2").first();
				Elements elmSecoStats = elmSeco.getElementsByClass("statmod-stat");

				int secoStatCnt = elmSecoStats.size();
				int idx = 0;
				String[] secoStatVal = new String[secoStatCnt];
				String[] secoStatTyp = new String[secoStatCnt];

				for (Element elmSecoStat : elmSecoStats) {
					secoStatVal[idx] = elmSecoStat.getElementsByClass("statmod-stat-value").first().text();
					secoStatTyp[idx] = elmSecoStat.getElementsByClass("statmod-stat-label").first().text();

					isPercent = false;
					if (secoStatVal[idx].indexOf("%") != -1) {
						isPercent = true;
					}
					mod.secStat[idx] = Mod.getSecondaryStatFromString(secoStatTyp[idx], isPercent);
					mod.secStatVal[idx] = Mod.getValueFromStringAsDouble(secoStatVal[idx]);
					idx++;
				}

				System.out.println(level + " : " + pips + " : " + toonName + " - " + modType + 
						" : " + primStatVal + " " + primStatTyp);

				for (int i = 0; i < secoStatCnt; i++) {
					System.out.print(secoStatVal[i] + " " + secoStatTyp[i] + ", ");
				}
				System.out.println();


				lsMods.add(mod);
			}

			System.out.println("Mods = " + lsMods.size());
		}



		return lsMods;
	}

}
